# BackofficeEmployees

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 17.2.3.

## Development server

1. Run `ng serve` or `npm start` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

2. Run JSON Server, `npm run json-start`
Endpoints for data employee using JSON-server:
`localhost:3000/posts`
`localhost:3000/comments`
`localhost:3000/profile`
`localhost:3000/users`
`localhost:3000/employees`

# For the login part, users cannot log in other than using the user data that has been provided.

For login:
email: `user@gmail.com`
Password: `user123`
![alt text](image.png)

  "users":
  [
    {
      "id": "1",
      "userName": "userTry",
      "email": "user@gmail.com",
      "password": "user123"
    }
  ]

## Dashboard
![alt text](image-2.png)

## Birth Date Validation
Date of birth must not exceed 18 years of age
![alt text](image-3.png)


## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
