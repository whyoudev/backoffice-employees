import { Component } from '@angular/core';
import { CoreService } from '../../core/core.service';
import { EmployeeService } from '../../services/employee.service';

@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrl: './confirm-dialog.component.scss'
})
export class ConfirmDialogComponent {
  // constructor (
  //   private _coreService: CoreService,
  //   private _empService: EmployeeService,
  // ){}

  // getEmployeeList() {
  //   this._empService.getEmployeeList()
  //   .subscribe({
  //     next:(res) => {
  //       this.dataSource = new MatTableDataSource(res);
  //       this.dataSource.sort = this.sort;
  //       this.dataSource.paginator = this.paginator;
  //     },
  //     error: (err) => {
  //       console.log(err)
  //     }
  //   })
  // }

  // confirmDeleteEmployee(id: number) {
  //   this._empService.deleteEmployee(id).subscribe({
  //     next: (res) => {
  //       this._coreService.openSnackBar('Employee deleted!', '🎊');
  //       this.getEmployeeList();
  //     },
  //     error: (err: any) => {
  //       console.error(err)
  //     }
  //   })
  // }
}
