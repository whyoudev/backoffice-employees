import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { EmployeeService } from '../services/employee.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { CoreService } from '../core/core.service';
import { group } from '@angular/animations';

interface Gr {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrl: './employees.component.scss'
})
export class EmployeesComponent implements OnInit {
  empComp: FormGroup

  groups: Gr[] = [
    { value: 'gr-0', viewValue: 'Teller' },
    { value: 'gr-1', viewValue: 'Customer Service' },
    { value: 'gr-2', viewValue: 'Back Office' },
    { value: 'gr-3', viewValue: 'Account Officer' },
    { value: 'gr-4', viewValue: 'Sales Officer' },
    { value: 'gr-5', viewValue: 'Staf Administration' },
    { value: 'gr-6', viewValue: 'Credit Analyst' },
    { value: 'gr-7', viewValue: 'Investment Banker' },
    { value: 'gr-8', viewValue: 'Management' },
    { value: 'gr-9', viewValue: 'Head Branch' },
    { value: 'gr-10', viewValue: 'Information Technology' },
  ];

  constructor(
    private _fb: FormBuilder,
    private _empService: EmployeeService,
    private _dialogRef: MatDialogRef<EmployeesComponent>,
    private _coreService: CoreService,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {
    this.initializeform();
    this.empComp = this._fb.group({
      userName: ['', Validators.required],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', Validators.required],
      birthDate: ['', Validators.required],
      basicSalary: ['', Validators.required],
      status: ['', Validators.required],
      group: ['', Validators.required],
      description: ['', Validators.required],
    })
  }

  initializeform(){
    this.empComp = new FormGroup({
      userName: new FormControl(),
      firstName: new FormControl(),
      lastName: new FormControl(),
      email: new FormControl(),
      birthDate: new FormControl(),
      basicSalary: new FormControl(),
      status: new FormControl(),
      group: new FormControl(),
      description: new FormControl()
    })
  }
  maxDate: Date;
  date: Date

  ngOnInit(): void {
    this.empComp.patchValue(this.data);
    this.maxDate = new Date();
    this.maxDate.setMonth(this.maxDate.getMonth() - 12 * 18);
  }



  onFormSubmit() {
    if (this.empComp.valid) {
      if (this.data) {
        this._empService.updateEmployee(this.data.id, this.empComp.value)
          .subscribe({
            next: (val: any) => {
              this._coreService.openSnackBar('Employee updated sucessfully!', '🤝');
              this._dialogRef.close(true);
            },
            error: (err: any) => {
              console.error(err)
            },
          });
      } else {
        this._empService.addEmployee(this.empComp.value)
          .subscribe({
            next: (val: any) => {
              this._coreService.openSnackBar('Employee added sucessfully!', '👍');
              this._dialogRef.close(true);
            },
            error: (err: any) => {
              console.error(err)
            },
          });
      }
    }
  }
}
